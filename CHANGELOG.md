Version 1.4.3
=============
- Add GPOS kerning rules for specific characters (imported from source, thanks to FontForge)
- Remove GSUB/GPOS lookups included in source (move to feature file)
- Add missing glyphs for ഷ്ക (shk1) and ഷ്ക്ര (shk1r1)
- Corrected glyph names of ഷ്കു (shk1u1) and ഷ്കൂ (shk1u2)
- Fix shaping of യ്യു, വ്വു with Lipika

Version 1.4.2
=============
- Fix shaping of യ്തു/y1th1u1 in InDesign

Version 1.4.1
=============
- Fix name in fontconfig

Version 1.4
===========
- Support old shapers from Windwos XP, old Pango, Lipika (Adobe) to HarfBuzz, Uniscribe

Version 1.3
===========
- Add RIT prefix to font name

Version 1.2
===========
- Build: shrink OTF size, and fix version check. Also add additional packages requierd for CI
- [FIX] glyph name of ഷ്ര , and add missing shaping rule for it. Thanks to test script for identifying it.
- Add test to check if glyphs are assigned either a Unicode codepoint or a shaping rule
- Add fontfonfig file for Panmana
- AppStream metadata: move to subdirectory

Version 1.1
===========
- GitLab: automated release management
- Makefile: updated build directory & release targets; and adjusted TeX test file
- [FIX] Build script: generated OTF file size can be greatly reduced via the 'round' option
- [MAJOR] Rewritten Malayalam shaping rules: form post-base u1/u2 forms of Ra, below-base La first and then the rest of the conjuncts. This fixes a large chunk of issues with limited character set fonts - they now form Ra forms consistently

Version 1.0
===========
- Add GitLab CI/CD
- Fix font name in Makefile
- Fix glyph of ngngui (it was same as ngngu2)
- Remove test pdf from version control
- Add License file
- Updated description
- Add AppStream metadata
- Add description of font and details of dedication
- Add test cases
- Build tools
- OpenType shaping rules
- source: Panmana → Panmana-Regular
- Fix metadata: font names, weight in OS/2 & Panose, TTF names etc.
- Revision 10
- Panmana development version 6
